Vue.createApp({ 
    async mounted() {
        try {
            await this.getEnv();
            this.getItems();
        }
        catch (error) {
            console.error(error);
        }
    },
    methods: {
        logout() {
            window.location.href = " http://localhost:8080/auth/realms/demo/protocol/openid-connect/logout?redirect_uri=encodedRedirectUri ";
            setTimeout(() => { window.location.href="http://localhost:8082"; }, 20);
        }
    }
}).mount("#app");