## Link to Keycloak Docker Image used
https://hub.docker.com/r/jboss/keycloak/

## To initialize Keycloak
docker run --name keycloak --net keycloak-network -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -p 8080:8080 jboss/keycloak

## To initilize Keycloak Backend
docker run --name mysql -d --net keycloak-network -e MYSQL_DATABASE=keycloak -e MYSQL_USER=keycloak -e MYSQL_PASSWORD=password -e MYSQL_ROOT_PASSWORD=root_password mysql

Both will be on the "keycloak-network" network by default.

## To get authentification token
This ![POST](Post_Request.bmp) request can be used to get an authentification token,

This was done using Postman. One important note is that the user password used to get the authentification token should be changed in the final implementation.

From here I planned on following the methods put forth in the following links which may be useful to any who try to implement this later.
- https://stackoverflow.com/questions/55535440/how-to-get-users-from-keycloak-rest-api/55539390
- https://developers.redhat.com/blog/2020/11/24/authentication-and-authorization-using-the-keycloak-rest-api/

## Inventory System Integration Plan
The following is a checklist that has steps for integrating Keycloak with the Inventory System.

- [ ] Write docker-compose file for Keycloak, otherwise use commands listed to run.
- [ ] Insert keycloak.json from in the Files_For_Integration folder into the src directory of each frontend.
- [ ] Insert keycloakCommands.js from the Files_For_Integration folder into the src directory of each frontend.
- [ ] Insert the code from the script.js in the Files_For_Integration folder to the Vue script file for the frontend.

This will start up a Keycloak instance can then be accessed at port 8080, with a persistent backend. From here you can import the demo realm by using the realm-export.json in the Files_For_Integration folder the first time Keycloak is run. After this the current deployment of Keycloak will be fully implemented into the system.
